
public class Scoreboard {
	int playerA;
	int playerB;

	private static final String[][] SCORES;
	static {
		SCORES = new String[][] { 
				{ "love-love"	, "love-fifteen"	, "love-thirty"		, "love-fourty" 	},
				{ "fifteen-love", "fifteen-fifteen"	, "fifteen-thirty"	, "fifteen-fourty" 	},
				{ "thirty-love"	, "thirty-fifteen"	, "thirty-thirty"	, "thirty-fourty"	},
				{ "fourty-love"	, "fourty-fifteen"	, "fourty-thirty"	, "duce" 			} };
	}

	public String getScore() {
		if (playerA >= 4 && playerA - playerB >= 2) {
			return "player A won";
		} else if (playerA - playerB == 1) {
			return "advantage A";
		}
		
		if (playerB >= 4 && playerB - playerA >= 2) {
			return "player B won";
		} else if (playerB - playerA == 1) {
			return "advantage B";
		}
		
		return SCORES[playerA][playerB];

	}

	public void playerAScores() {
		playerA++;
	}

	public void playerBScores() {
		playerB++;
	}

}
