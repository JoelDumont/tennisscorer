import static org.junit.Assert.assertEquals;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

public class TennisScorerTest {

	@Test
	public void initialScoreboardState() {
		Scoreboard board = new Scoreboard();

		String boardState = board.getScore();

		assertEquals("love-love", boardState);
	}
	
	@Test
	public void firstHappyCase() {
		Scoreboard board = new Scoreboard();

		scoreBuilder(board,"AAAB");
		String boardState = board.getScore();

		assertEquals("fourty-fifteen", boardState);
	}

	@Test
	public void AScoresFourBScoresTwoRetrunAWon() {
		Scoreboard board = new Scoreboard();

		scoreBuilder(board,"AAAABB");
		String boardState = board.getScore();

		assertEquals("player A won", boardState);
	}

	@Test
	public void AScoresFourBScoresFourRetrunDuce() {
		Scoreboard board = new Scoreboard();

		scoreBuilder(board,"AAABBB");
		String boardState = board.getScore();
		
		assertEquals("duce", boardState);
	}
	
	@Test
	public void AScoresFiveBScoresThreeReturnAWon() {
		Scoreboard board = new Scoreboard();

		scoreBuilder(board,"AAAABB");
		String boardState = board.getScore();
		
		assertEquals("player A won", boardState);
	}
	
	@Test
	public void AScoresThreeBScoresFiveReturnBWon() {
		Scoreboard board = new Scoreboard();

		scoreBuilder(board,"AABBBB");
		String boardState = board.getScore();
		
		assertEquals("player B won", boardState);
	}
	
	@Test
	public void AScoresFourBScoresThreeReturnAWon() {
		Scoreboard board = new Scoreboard();

		scoreBuilder(board,"AAAABBB");
		String boardState = board.getScore();
		
		assertEquals("advantage A", boardState);
	}
	
	@Test
	public void BScoresFourAScoresThreeReturnAWon() {
		Scoreboard board = new Scoreboard();

		scoreBuilder(board,"AAABBBB");
		String boardState = board.getScore();
		
		assertEquals("advantage B", boardState);
	}
	
	@Test
	public void BScoresEightAScoresSevenReturnAWon() {
		Scoreboard board = new Scoreboard();

		scoreBuilder(board,"AAAAAAABBBBBBBB");
		String boardState = board.getScore();
		
		assertEquals("advantage B", boardState);
	}
	
	
	// Builder Methods
	
	public void scoreBuilder(Scoreboard board, String points) {
		scoreFor(board, points, 'A');
		scoreFor(board, points, 'B');
	}

	private void scoreFor(Scoreboard board, String points, char player) {
		Pattern pattern = Pattern.compile("[" + player + "]");
		Matcher matcher = pattern.matcher(points);
		while (matcher.find()) {
			if (player == 'A') {
				board.playerAScores();
			} else if (player == 'B') {
				board.playerBScores();
			}
		}
	}
}
