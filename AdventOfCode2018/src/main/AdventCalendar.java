package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class AdventCalendar {
	private List<String> values = new LinkedList<>();

	public void importData(File file) {
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
			String next;
			while ((next = reader.readLine()) != null) {
				values.add(next);
			}
		} catch (FileNotFoundException e) {
			System.out.println("advent_01.txt not found");
			e.printStackTrace();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List<Integer> getInputData() {
		return values.stream().map(s -> Integer.parseInt(s)).collect(Collectors.toList());
	}

	public int calcFrequencyChanges() {
		return values.stream().map(s -> Integer.parseInt(s)).mapToInt(i -> i).sum();
	}

	public int getFirstFrequencyReachedTwice() {
		List<Integer> intValues = values.stream().map(s -> Integer.parseInt(s)).collect(Collectors.toList());
		List<Integer> processedFrequencies = new LinkedList<>();
		int result = 0;
		outer: while (true) {
			for (Integer value : intValues) {
				result += value;
				if (processedFrequencies.contains(result)) {
					break outer;
				} else {
					processedFrequencies.add(result);
				}
			}
		}
		return result;
	}

	public int calcChecksum() {
		Pattern p = Pattern.compile(".    $");
		Matcher m = p.matcher("aaaaab");
		boolean b = m.matches();
		return 0;
	}

}
