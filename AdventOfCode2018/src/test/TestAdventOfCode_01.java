package test;


import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import main.AdventCalendar;

public class TestAdventOfCode_01 {
	
	// TODO
    //
	
	private final String path_01 = "C:\\Users\\joeldumont\\Documents\\LöPaAssessment\\AdventOfCode2018\\resources\\advent_01.txt";
	private final String path_02 = "C:\\Users\\joeldumont\\Documents\\LöPaAssessment\\AdventOfCode2018\\resources\\advent_02.txt";
	
	@Test
	public void readInputFile() {
		File file = new File(path_01);
		AdventCalendar calendar = new AdventCalendar();
		
		calendar.importData(file);
		
		assertFirstThreeElements(calendar);
	}
	
	@Test
	public void minusTwoPlusThreeReturnsOne() {
		File file = new File(path_01);
		AdventCalendar calendar = new AdventCalendar();		
		calendar.importData(file);
		
		int sum = calendar.calcFrequencyChanges();
		
		assertEquals(585,sum);
	}
	
//	@Test
//	public void firstFrequencyReachedTwice() {
//		File file = new File(path_01);
//		AdventCalendar calendar = new AdventCalendar();		
//		calendar.importData(file);
//		
//		int frequency = calendar.getFirstFrequencyReachedTwice();
//		
//		assertEquals(83173,frequency);
//	}
	
	@Test
	public void calculateChecksumOfFirstEntry() {
		File file = new File(path_02);
		AdventCalendar calendar = new AdventCalendar();		
		calendar.importData(file);
		
		int checkusm = calendar.calcChecksum();
		
		assertEquals(0,checkusm);
	}
	
	
	// ------------- Helper Classes ------------------------
	
	private void assertFirstThreeElements(AdventCalendar calendar) {
		List<Integer> result = new ArrayList<>();
		result.add(16);
		result.add(-2);
		result.add(-5);
		assertEquals(result.get(0),calendar.getInputData().get(0));
		assertEquals(result.get(1),calendar.getInputData().get(1));
		assertEquals(result.get(2),calendar.getInputData().get(2));
	}


}
